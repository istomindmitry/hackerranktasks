package thirtydayscodechallenge.task26;

import java.io.*;

public class Solution {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] a = reader.readLine().split(" ");
        String[] b = reader.readLine().split(" ");
        int[] expecteddate = new int[3];
        int[] returndate = new int[3];
        for (int i = 0; i < 3; i++) {
            returndate[i] = Integer.parseInt(a[i]);
            expecteddate[i] = Integer.parseInt(b[i]);

        }
        if (expecteddate[2] == returndate[2]) {
            if (expecteddate[1] == returndate[1]) {
                if ((returndate[0] - expecteddate[0]) * 15 < 0)
                    System.out.println(0);
                else
                    System.out.println((returndate[0] - expecteddate[0]) * 15);

            } else {
                if (500 * (returndate[1] - expecteddate[1]) < 0)
                    System.out.println(0);
                else
                    System.out.println(500 * (returndate[1] - expecteddate[1]));
            }

        } else {
            if (expecteddate[2] > returndate[2])
                System.out.println(0);
            else
                System.out.println(10000);
        }
    }
}