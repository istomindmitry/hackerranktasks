package thirtydayscodechallenge.task10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task10 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        String s = "";
        while (n > 0) {
            s += n % 2;
            n /= 2;
        }
        int output = 0;
        String[] array = s.split("0");
        for (int i = 0; i < array.length; i++) {
            if (output < array[i].length())
                output = array[i].length();
        }
        System.out.println(output);
    }
}
