package thirtydayscodechallenge.task6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task6 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(reader.readLine());
        String[] output = new String[T];
        for (int i = 0; i < T; i++) {
            char[] a = reader.readLine().toCharArray();
            output[i] = "";
            for (int j = 0; j < a.length; j += 2) {
                output[i] += a[j];
            }
            output[i] += " ";
            for (int j = 1; j < a.length; j += 2) {
                output[i] += a[j];
            }
        }
        for (int i = 0; i < output.length; i++)
            System.out.println(output[i]);
    }
}
