package thirtydayscodechallenge.task8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Task8 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            String[] a = reader.readLine().split(" ");
            map.put(a[0], a[1]);
        }
        while (reader.ready()) {
            String s = reader.readLine();
            if (map.containsKey(s)) {
                System.out.println(s + "=" + map.get(s));
            } else {
                System.out.println("Not found");
            }
        }
    }
}
