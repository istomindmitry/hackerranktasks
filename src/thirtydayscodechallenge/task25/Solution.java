package thirtydayscodechallenge.task25;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(reader.readLine());
        for (int i = 0; i < T; i++) {
            int n = Integer.parseInt(reader.readLine());
            if (isPrime(n))
                System.out.println("Prime");
            else
                System.out.println("Not prime");
        }
    }

    private static boolean isPrime(int n) {
        if(n == 1 ) return false;
        for (int i = 2; i <= n / 2; i++)
            if (n % i == 0)
                return false;
        return true;
    }
}
