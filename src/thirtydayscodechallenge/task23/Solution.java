package thirtydayscodechallenge.task23;

import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    static void levelOrder(Node root) {
        //Write your code here
        ArrayList<Node> thislevel = new ArrayList<>();
        ArrayList<Node> nextlevel = new ArrayList<>();
        thislevel.add(root);
        boolean a = true;
        while (a) {
            a = false;
            for (int i = 0; i < thislevel.size(); i++) {
                Node node = thislevel.get(i);
                System.out.print(node.data + " ");
                if (node.left != null) {
                    nextlevel.add(node.left);
                    a = true;
                }
                if (node.right != null) {
                    nextlevel.add(node.right);
                    a = true;
                }
            }
            thislevel = nextlevel;
            nextlevel = new ArrayList<>();
        }
    }

    public static Node insert(Node root, int data) {
        if (root == null) {
            return new Node(data);
        } else {
            Node cur;
            if (data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        Node root = null;
        while (T-- > 0) {
            int data = sc.nextInt();
            root = insert(root, data);
        }
        levelOrder(root);
    }
}

class Node {
    Node left, right;
    int data;

    Node(int data) {
        this.data = data;
        left = right = null;
    }
}
