package thirtydayscodechallenge.task18;

import java.util.*;

public class Solution {
    LinkedList<Character> queue = new LinkedList<>();
    Stack<Character> stack = new Stack<>();

    public void pushCharacter(char c)
    {
        stack.push(c);
    }

    public void enqueueCharacter(char c)
    {
        queue.add(c);
    }

    public char popCharacter()
    {
        return stack.pop();
    }

    public char dequeueCharacter()
    {
        char c = queue.get(0);
        queue.remove(0);
        return c;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        scan.close();

        // Convert input String to an array of characters:
        char[] s = input.toCharArray();

        // Create a Generics object:
        Solution p = new Solution();

        // Enqueue/Push all chars to their respective data structures:
        for (char c : s) {
            p.pushCharacter(c);
            p.enqueueCharacter(c);
        }

        // Pop/Dequeue the chars at the head of both data structures and compare them:
        boolean isPalindrome = true;
        for (int i = 0; i < s.length / 2; i++) {
            if (p.popCharacter() != p.dequeueCharacter()) {
                isPalindrome = false;
                break;
            }
        }

        //Finally, print whether string s is palindrome or not.
        System.out.println("The word, " + input + ", is "
                + ((!isPalindrome) ? "not a palindrome." : "a palindrome."));
    }
}
