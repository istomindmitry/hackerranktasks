package thirtydayscodechallenge.task9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;

public class Task9 {

    static int factorial(int n) {
        // Complete this function
        if (n < 2) return 1;
        return factorial(n - 1) * n;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int result = factorial(n);
        System.out.println(result);
    }


    }



