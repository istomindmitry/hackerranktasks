package thirtydayscodechallenge.task12;

class Student extends Person {
    private int[] testScores;

    /*
     *   Class Constructor
     *
     *   @param firstName - A string denoting the Person's first name.
     *   @param lastName - A string denoting the Person's last name.
     *   @param id - An integer denoting the Person's ID number.
     *   @param scores - An array of integers denoting the Person's test scores.
     */
    public Student(String firstName, String lastName, int id, int[] scores) {
        super(firstName, lastName, id);
        this.testScores = scores;
    }

    /*
     *   Method Name: calculate
     *   @return A character denoting the grade.
     */
    // Write your method here
    public char calculate() {
        double avGrade = 0;
        double sum = 0;
        for (int i = 0; i < testScores.length; i++)
            sum += testScores[i];
        avGrade = sum / testScores.length;
        if (avGrade >= 90)
            return 'O';
        if (avGrade >= 80)
            return 'E';
        if (avGrade >= 70)
            return 'A';
        if (avGrade >= 55)
            return 'P';
        if (avGrade >= 40)
            return 'D';
        return 'T';
    }
}